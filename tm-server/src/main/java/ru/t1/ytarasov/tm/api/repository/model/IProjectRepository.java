package ru.t1.ytarasov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @Nullable
    List<Project> findAll(@NotNull final Comparator comparator);

    @Nullable
    List<Project> findAll(@NotNull final String userId, @NotNull final Comparator comparator);

}
