package ru.t1.ytarasov.tm.service;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.IPropertyService;
import ru.t1.ytarasov.tm.dto.model.ProjectDTO;
import ru.t1.ytarasov.tm.dto.model.SessionDTO;
import ru.t1.ytarasov.tm.dto.model.TaskDTO;
import ru.t1.ytarasov.tm.dto.model.UserDTO;
import ru.t1.ytarasov.tm.listener.EntityListener;
import ru.t1.ytarasov.tm.log.JmsLoggerProducer;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.model.Session;
import ru.t1.ytarasov.tm.model.Task;
import ru.t1.ytarasov.tm.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NotNull IPropertyService propertyService) {
        this.propertyService = propertyService;
        this.entityManagerFactory = factory();
    }

    @Override
    public @NotNull EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, propertyService.getDBDriver());
        settings.put(Environment.URL, propertyService.getDBUrl());
        settings.put(Environment.USER, propertyService.getDBUsername());
        settings.put(Environment.PASS, propertyService.getDBPassword());
        settings.put(Environment.DIALECT, propertyService.getDBDialect());
        settings.put(Environment.HBM2DDL_AUTO, propertyService.getDBHbm2ddlAuto());
        settings.put(Environment.SHOW_SQL, propertyService.getShowSql());
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(Task.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @Override
    public @NotNull EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @Override
    public void initLogger() {
        @NotNull final JmsLoggerProducer jmsLoggerProducer = new JmsLoggerProducer();
        @NotNull final EntityListener entityListener = new EntityListener(jmsLoggerProducer);
        @NotNull final SessionFactoryImpl sessionFactory =
                entityManagerFactory.unwrap(SessionFactoryImpl.class);
        @NotNull final EventListenerRegistry eventListenerRegistry =
                sessionFactory.getServiceRegistry().getService(EventListenerRegistry.class);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_INSERT).appendListener(entityListener);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_UPDATE).appendListener(entityListener);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_DELETE).appendListener(entityListener);
    }

}
